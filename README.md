# Regen script
A fork of [DisorderX's health regen script](https://www.gtagarage.com/mods/show.php?id=11645).

### Changes:
* Optimised to reduce CPU load
* Stripped broken player animation feature
* Made configurable with variables for regen amount and interval
* Now prevents regen if player has taken damage since the last check (you can't regen mid-combat)
* Bug fixes and code cleanup

<br />

## Installation:
1. Install ScriptHookDotNet script loader
2. Add `regen.cs` to your GTAIV's scripts folder (create one if none exists)